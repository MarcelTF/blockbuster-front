import { Component, OnInit, ComponentFactoryResolver } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from {./LoginService };
import { Cliente } from '../clientes/cliente';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: any;

  constructor(
    private router: Router
    ) { }

  ngOnInit(): void {
    this.credentials = {username: '', password: ''};
  }

  login(): void {
    console.log(this.credentials);
    this.loginService.save(credentials);
    this.router.navigate(['/companias']);
  }
}
