import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AlertComponent } from './alert/alert.component';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component'
import { FooterComponent } from "./footer/footer.component";
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteService } from './clientes/cliente.service';
import { ClientesFormComponent } from './clientes/form.component';
import { JuegosComponent } from './juegos/juegos.component';
import { JuegoService } from './juegos/juego.service';
import { JuegosFormComponent } from './juegos/form.component';
import { StocksComponent } from './stocks/stocks.component';
import { TiendasComponent } from './tiendas/tiendas.component';
import { CompaniasComponent } from './companias/companias.component';
import { LoginComponent } from './login/login.component';

const ROUTES: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'clientes', component: ClientesComponent },
  { path: 'clientes/form', component: ClientesFormComponent },
  { path: 'clientes/form/:id', component: ClientesFormComponent },
  { path: 'juegos', component: JuegosComponent },
  { path: 'juegos/form', component: JuegosFormComponent },
  { path: 'juegos/form/:id', component: JuegosFormComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ClientesComponent,
    ClientesFormComponent,
    JuegosComponent,
    JuegosFormComponent,
    AlertComponent,
    StocksComponent,
    TiendasComponent,
    CompaniasComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),
    FormsModule
  ],
  providers: [ClienteService, JuegoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
