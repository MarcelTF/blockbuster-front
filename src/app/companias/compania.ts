import { Juego } from '../juegos/juego';

export class Compania {
    id: number;
    cif: string;
    name: string;

    juegos: Juego[];
}