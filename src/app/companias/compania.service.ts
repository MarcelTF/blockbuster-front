import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AlertService } from '../alert/alert.service';
import { Compania } from './compania';


@Injectable({
  providedIn: 'root'
})
export class CompaniaService {

  private urlServer: string ='http://localhost:8090/api/';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/Json'});

  constructor(private http: HttpClient,  private router: Router, private alertService: AlertService) { }

  getCompanias(): Observable<Compania[]> { 
      return this.http.get<Compania[]>(`${this.urlServer}companias`).pipe(
      catchError(error => {
        this.alertService.error(`Error al consultar las compañias: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  createCompania(compania: Compania): Observable<Compania> { 
    return this.http.post<Compania>(`${this.urlServer}companias`, compania, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        this.alertService.error(`Error al añadir la compañia: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  updateCompania(compania: Compania): Observable<Compania> { 
    return this.http.put<Compania>(`${this.urlServer}companias/${compania.id}`, compania, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        this.alertService.error(`Error al actualizar la compañia: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  getCompania(id: number): Observable<Compania> { 
    return this.http.get<Compania>(`${this.urlServer}companias/${id}`).pipe(
      catchError(error => {
        this.alertService.error(`Error al consultar la compañia: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  deleteCompania(id: number): Observable<any> { 
    return this.http.delete<Compania>(`${this.urlServer}companias/${id}`).pipe(
      catchError(error => {
        this.alertService.error(`Error al borrar la compañia: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }
}
