import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert/alert.service';

import { Compania } from './compania';
import { CompaniaService } from './compania.service';

@Component({
  selector: 'app-companias',
  templateUrl: './companias.component.html'
})
export class CompaniasComponent implements OnInit {

  companias: Compania[];
  showId: boolean=true;

  constructor(private companiaService: CompaniaService, private alertService: AlertService) { }

  ngOnInit() {
    this.refreshCompanias();
  }

  switchId(): void {
    this.showId = !this.showId;
  }

  delete(id: number): void {
    if (confirm(`¿Está seguro que desea eliminar la compañia ${id}?`)) {
      this.companiaService.deleteCompania(id).subscribe(response => { 
        this.alertService.success(`Se ha eliminado correctamente la compañia con ID: ${id}`, {autoClose: true});
        this.refreshCompanias();
      });
    }
  }

  refreshCompanias(): void {
    this.companiaService.getCompanias().subscribe(
      companias => this.companias = companias
    );
  }
}

