import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert/alert.service';

import { Tienda } from './tienda';
import { TiendaService } from './tienda.service';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html'
})
export class TiendasComponent implements OnInit {

  tiendas: Tienda[];
  showId: boolean=true;

  constructor(private tiendaService: TiendaService, private alertService: AlertService) { }

  ngOnInit() {
    this.refreshTiendas();
  }

  switchId(): void {
    this.showId = !this.showId;
  }

  delete(id: number): void {
    if (confirm(`¿Está seguro que desea eliminar el juego ${id}?`)) {
      this.tiendaService.deleteTienda(id).subscribe(response => { 
        this.alertService.success(`Se ha eliminado correctamente el juego con ID: ${id}`, {autoClose: true});
        this.refreshTiendas();
      });
    }
  }

  refreshTiendas(): void {
    this.tiendaService.getTiendas().subscribe(
      tiendas => this.tiendas = tiendas
    );
  }

}
