import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from '../alert/alert.service'
import { Tienda } from './tienda';

@Injectable({
  providedIn: 'root'
})
export class TiendaService {

  private urlServer: string ='http://localhost:8090/api/';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/Json'});

  constructor(private http: HttpClient,  private alertService: AlertService) { }

  getTiendas(): Observable<Tienda[]> { 
    return this.http.get<Tienda[]>(`${this.urlServer}tiendas`).pipe(
      catchError(error => {
        this.alertService.error(`Error al consultar las tiendas: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  createTienda(tienda: Tienda): Observable<Tienda> { 
    return this.http.post<Tienda>(`${this.urlServer}tiendas`, tienda, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        this.alertService.error(`Error al añadir la tienda: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  updateTiendas(tienda: Tienda): Observable<Tienda> { 
    return this.http.put<Tienda>(`${this.urlServer}tiendas/${tienda.id}`, tienda, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        this.alertService.error(`Error al actualizar la tienda: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  getTienda(id: number): Observable<Tienda> { 
    return this.http.get<Tienda>(`${this.urlServer}tiendas/${id}`).pipe(
      catchError(error => {
        this.alertService.error(`Error al consultar la tienda: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  deleteTienda(id: number): Observable<any> { 
    return this.http.delete<Tienda>(`${this.urlServer}tiendas/${id}`).pipe(
      catchError(error => {
        this.alertService.error(`Error al borrar la tienda: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }
}
