import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';
import { AlertService } from '../alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-juegos',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class JuegosFormComponent implements OnInit {

  juego: Juego = new Juego();
  categorias: any[] = [ 
    { title: 'Acción', value: 'ACTION' }, 
    { title: 'Shooter', value: 'SHOOTER' },
    { title: 'Estrategia', value: 'ESTRATEGIA' },
    { title: 'Deporte', value: 'DEPORTE' },
    { title: 'Plataformas', value: 'PLATAFORMAS' },
    { title: 'Carrera', value: 'CARRERAS' },
    { title: 'Aventura', value: 'AVENTURA' },
    { title: 'Rol', value: 'ROL' }];
  title: string;

  constructor(
    private juegoService: JuegoService, 
    private alertService: AlertService, 
    private router: Router, 
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {
    this.loadJuego();
  }

  create(): void {
    this.juegoService.createJuego(this.juego).subscribe(
      juego => {
        this.alertService.success(`Se ha creado correctamente el juego "${juego.title}" con ID: ${juego.id}`, {autoClose: true});
        this.router.navigate(['/juegos']);
      }
    )
  }

  loadJuego(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.title = 'Editar Juego';
        this.juegoService.getJuego(id).subscribe(
          juego => this.juego = juego
        )
      } else {
        this.title = 'Crear Juego'
      }
    })
  } 

  update(): void {

  }

}
