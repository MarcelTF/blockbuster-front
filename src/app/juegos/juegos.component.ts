import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert/alert.service';

import { Juego } from './juego';
import { JuegoService } from './juego.service';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html'
})
export class JuegosComponent implements OnInit {

  juegos: Juego[];
  showId: boolean=true;

  constructor(private juegoService: JuegoService, private alertService: AlertService) { }

  ngOnInit() {
    this.refreshJuegos();
  }

  switchId(): void {
    this.showId = !this.showId;
  }

  delete(id: number): void {
    if (confirm(`¿Está seguro que desea eliminar el juego ${id}?`)) {
      this.juegoService.deleteJuego(id).subscribe(response => { 
        this.alertService.success(`Se ha eliminado correctamente el juego con ID: ${id}`, {autoClose: true});
        this.refreshJuegos();
      });
    }
  }

  refreshJuegos(): void {
    this.juegoService.getJuegos().subscribe(
      juegos => this.juegos = juegos
    );
  }
}
