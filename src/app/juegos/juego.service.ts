import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AlertService } from '../alert/alert.service'
import { Juego } from './juego';

@Injectable()
export class JuegoService {

  private urlServer: string ='http://localhost:8090/api/';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/Json'});

  constructor(private http: HttpClient,  private alertService: AlertService, private router: Router) { }

  getJuegos(): Observable<Juego[]> { 
    return this.http.get<Juego[]>(`${this.urlServer}juegos`).pipe(
      catchError(error => {
        console.error(`getJuegos error: "${error.message}"`);
        if (error.status == 401) {
           this.router.navigate(['/login']);
        } else {
          this.alertService.error(`Error al consultar los juegos: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        }
        return throwError(error);
      })
    );
  }

  createJuego(juego: Juego): Observable<Juego> { 
    return this.http.post<Juego>(`${this.urlServer}juegos`, juego, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        console.error(`updateJuego error: "${error.message}"`);
        this.alertService.error(`Error al crear el juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  updateJuegos(juego: Juego): Observable<Juego> { 
    return this.http.put<Juego>(`${this.urlServer}juegos/${juego.id}`, juego, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        console.error(`updateJuego error: "${error.message}"`);
        if (error.status == 400) {
          error.error.errorMessage.replace('[', '').replace(']', '').split(', ').reverse().forEach(errorMessage => {
            this.alertService.error(errorMessage);
          });
        } else {
          this.alertService.error(`Error al actualizar el juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        }
        
        return throwError(error);
      })
    );
  }

  getJuego(id: number): Observable<Juego> { 
    return this.http.get<Juego>(`${this.urlServer}juegos/${id}`).pipe(
      catchError(error => {
        console.error(`getJuego error: "${error.message}"`);
        this.alertService.error(`Error al consultar el juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  deleteJuego(id: number): Observable<any> { 
    return this.http.delete<Juego>(`${this.urlServer}juegos/${id}`).pipe(
      catchError(error => {
        console.error(`deleteJuego error: "${error.message}"`);
        this.alertService.error(`Error al borrar el juego: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

}
