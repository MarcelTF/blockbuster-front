import { Compania } from '../companias/compania';

export class Juego {
    id: number;
    title: string;
    launchDate: string;
    category: string;
    price: number;
    pegi: number;

    companies: Compania[];
}
