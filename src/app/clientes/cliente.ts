export class Cliente {
    id: number;
    name: string;
    lastname: string;
    username: string;
    password: string;
    dni: string;
    email: string;
    birthday: string; 
}
