import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { AlertService } from '../alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-clientes',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class ClientesFormComponent implements OnInit {

  cliente: Cliente = new Cliente();
  title: string;

  constructor(
    private clienteService: ClienteService, 
    private alertService: AlertService, 
    private router: Router, 
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadCliente();
  }

  create(): void {
    this.clienteService.createCliente(this.cliente).subscribe(
      cliente => {
        this.alertService.success(`Se ha creado correctamente el cliente "${cliente.name}" con ID: ${cliente.id}`, {autoClose: false});
        this.router.navigate(['/clientes']);
      }
    )
  }

  loadCliente(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.title = 'Editar Cliente';
        this.clienteService.getCliente(id).subscribe(
          cliente => this.cliente = cliente
        )
      } else {
        this.title = 'Crear Cliente'
      }
    })
  } 

  update(): void {

  }

}
