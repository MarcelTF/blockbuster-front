import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AlertService } from '../alert/alert.service';

import { Cliente } from './cliente';

@Injectable()
export class ClienteService {

  private urlServer: string ='http://localhost:8090/api/';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/Json'});

  constructor(private http: HttpClient,  private router: Router, private alertService: AlertService) { }

  getClientes(): Observable<Cliente[]> { 
      return this.http.get<Cliente[]>(`${this.urlServer}clientes`).pipe(
      catchError(error => {
        if (error.status == 401) {
          this.router.navigate(['/login']);
       } else {
         this.alertService.error(`Error al consultar los clientes: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
       }
        return throwError(error);
      })
    );
  }

  createCliente(cliente: Cliente): Observable<Cliente> { 
    return this.http.post<Cliente>(`${this.urlServer}clientes`, cliente, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        this.alertService.error(`Error al añadir el cliente: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  updateCliente(cliente: Cliente): Observable<Cliente> { 
    return this.http.put<Cliente>(`${this.urlServer}clientes/${cliente.id}`, cliente, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        this.alertService.error(`Error al actualizar el cliente: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  getCliente(id: number): Observable<Cliente> { 
    return this.http.get<Cliente>(`${this.urlServer}clientes/${id}`).pipe(
      catchError(error => {
        this.alertService.error(`Error al consultar el cliente: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  deleteCliente(id: number): Observable<any> { 
    return this.http.delete<Cliente>(`${this.urlServer}clientes/${id}`).pipe(
      catchError(error => {
        this.alertService.error(`Error al borrar el cliente: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }
}
