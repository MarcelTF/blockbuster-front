import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert/alert.service';

import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html'
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  showId: boolean=true;

  constructor(private clienteService: ClienteService, private alertService: AlertService) { }

  ngOnInit() {
    this.refreshClientes();
  }

  switchId(): void {
    this.showId = !this.showId;
  }

  delete(id: number): void {
    if (confirm(`¿Está seguro que desea eliminar el cliente ${id}?`)) {
      this.clienteService.deleteCliente(id).subscribe(response => { 
        this.alertService.success(`Se ha eliminado correctamente el juego con ID: ${id}`, {autoClose: true});
        this.refreshClientes();
      });
    }
  }

  refreshClientes(): void {
    this.clienteService.getClientes().subscribe(
      clientes => this.clientes = clientes
    );
  }

}
