import { Component, OnInit } from '@angular/core';
import { AlertService } from '../alert/alert.service';

import { Stock } from './stock';
import { StockService } from './stock.service';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html'
})
export class StocksComponent implements OnInit {

  stocks: Stock[];
  showId: boolean=true;

  constructor(private stockService: StockService, private alertService: AlertService) { }

  ngOnInit() {
    this.refreshStocks();
  }

  switchId(): void {
    this.showId = !this.showId;
  }

  delete(id: number): void {
    if (confirm(`¿Está seguro que desea eliminar el stock ${id}?`)) {
      this.stockService.deleteStock(id).subscribe(response => { 
        this.alertService.success(`Se ha eliminado correctamente el stock con ID: ${id}`, {autoClose: true});
        this.refreshStocks();
      });
    }
  }

  refreshStocks(): void {
    this.stockService.getStocks().subscribe(
      stocks => this.stocks = stocks
    );
  }

}
