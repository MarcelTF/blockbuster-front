import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from '../alert/alert.service'
import { Stock } from './stock';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  private urlServer: string ='http://localhost:8090/api/';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/Json'});

  constructor(private http: HttpClient,  private alertService: AlertService) { }

  getStocks(): Observable<Stock[]> { 
    return this.http.get<Stock[]>(`${this.urlServer}stocks`).pipe(
      catchError(error => {
        this.alertService.error(`Error al consultar los juegos: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  createStock(stock: Stock): Observable<Stock> { 
    return this.http.post<Stock>(`${this.urlServer}stocks`, stock, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        this.alertService.error(`Error al añadir el stock: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  updateStocks(stock: Stock): Observable<Stock> { 
    return this.http.put<Stock>(`${this.urlServer}stocks/${stock.id}`, stock, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        this.alertService.error(`Error al actualizar el stock: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  getStock(id: number): Observable<Stock> { 
    return this.http.get<Stock>(`${this.urlServer}stocks/${id}`).pipe(
      catchError(error => {
        this.alertService.error(`Error al consultar el stock: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }

  deleteStock(id: number): Observable<any> { 
    return this.http.delete<Stock>(`${this.urlServer}stocks/${id}`).pipe(
      catchError(error => {
        this.alertService.error(`Error al borrar el stock: "${error.message}"`, {autoClose: false, keepAfterRouteChange: false}); 
        return throwError(error);
      })
    );
  }
}
